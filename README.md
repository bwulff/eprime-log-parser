# EPrime Log File Parser #

This is a small JS that reads the .txt log files that EPrime generates during an experiment run. It transcribes the data to CSV, so that it can be processed using a spreadsheet application.
